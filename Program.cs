﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp9
{
    public class ErrorLogger
    {
        private static readonly ErrorLogger instance = new ErrorLogger();
        private readonly List<ErrorEntry> errorLog;
        private readonly string logFilePath = "./error_log.txt";

        private ErrorLogger()
        {
            errorLog = new List<ErrorEntry>();
        }

        public static ErrorLogger Instance => instance;

        public void LogError(string code, string description)
        {
            errorLog.Add(new ErrorEntry
            {
                TimeStamp = DateTime.Now,
                ErrorCode = code,
                ErrorDescription = description
            });
        }

        public void DisplayErrorLog()
        {
            Console.WriteLine("Error Log:");
            foreach (var entry in errorLog)
            {
                Console.WriteLine($"{entry.TimeStamp} - [{entry.ErrorCode}] {entry.ErrorDescription}");
            }
        }

        public void SaveErrorLogToFile()
        {
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                foreach (var entry in errorLog)
                {
                    writer.WriteLine($"{entry.TimeStamp} - [{entry.ErrorCode}] {entry.ErrorDescription}");
                }
            }
            Console.WriteLine($"Error log saved to {logFilePath}");
        }
    }

    public class ErrorEntry
    {
        public DateTime TimeStamp { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            ErrorLogger errorLogger = ErrorLogger.Instance;

            errorLogger.LogError("ERR001", "Input data error");
            errorLogger.LogError("ERR002", "Server connection error");

            errorLogger.DisplayErrorLog();
            errorLogger.SaveErrorLogToFile();
        }
    }
}
